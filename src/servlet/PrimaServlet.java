package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@WebServlet("/PrimaServlet") //primo modo per importare la servlet
public class PrimaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append("ciao chicco");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().append("<!DOCTYPE html>")
		.append("<html lang=`it`>")
		.append("<head>")
		.append("<meta charset=\"UTF-8\">")
		.append("<title>Document</title>")
		.append("</head>")
		.append("<body style=\"display:flex; justify-content: center;\">")
		.append("<form action=\"UrlPrima\" method=\"POST\">")
		.append("<input type=\"text\" name=\"nome\" placeholder=\"inserisci il tuo nome\"></br>")
		.append("<input type=\"text\" name=\"cognome\" placeholder=\"inserisci il tuo cognome\"></br>")
		.append("<input type=\"submit\"></br>")
		.append("</form>")
		.append("</body>")
		.append("</html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response); //ricarica la stessa pagina per questa motivo
		/*dobbiamo prenderci i parametri html*/
		/*dal front end ritorno semprestringhe*/
		String nome = request.getParameter("nome").trim(); //nome nel form
		String cognome = request.getParameter("cognome").trim();
		
		ServletContext s = getServletContext();
		String nomeAdmin = s.getInitParameter("admin_name");
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter p = response.getWriter().append("<!DOCTYPE html>")
				.append("<html lang=`it`>")
				.append("<head>")
				.append("<meta charset=\"UTF-8\">")
				.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">")
				.append("<title>Document</title>")
				.append("</head>");
		
		if(nomeAdmin.equalsIgnoreCase(nome+"."+cognome)) {
			p.append("<h1> benvenuto mio signore e padrone </h1>");
		}else if((nome.length() > 0) && (cognome.length() > 0)) {
			p.append("<h1> benvenuto " + nome + "cognome " + cognome + "</h1>");
		}else {
			p.append("<h3>errore! inserisci campi validi</h3>");
		}
		p.append("</body>")
			.append("</html>");
		
		/*di norma doGet prendo i dati*/
		/*nel doPost li analizzo*/
		
	}

}
